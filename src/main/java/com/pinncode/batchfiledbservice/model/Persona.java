package com.pinncode.batchfiledbservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class Persona {

    private String primerNombre;
    private String segundoNombre;
    private String telefono;


    @Override
    public String toString() {
        return "Persona{" +
                "primerNombre='" + primerNombre + '\'' +
                ", segundoNombre='" + segundoNombre + '\'' +
                ", telefono='" + telefono + '\'' +
                '}';
    }
}
