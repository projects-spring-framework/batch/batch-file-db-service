package com.pinncode.batchfiledbservice.configuration;


import com.pinncode.batchfiledbservice.listener.JobListener;
import com.pinncode.batchfiledbservice.model.Persona;
import com.pinncode.batchfiledbservice.processor.PersonaItemProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;

@Slf4j
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    /**
     * Reader que obtiene la informacion del archivo sample-data.csv
     * Asigna los valores a las columnas 'name', 'date'
     * @return
     */
    @Bean
    public FlatFileItemReader<Persona> reader () {

        log.info("Step 2 ::::::::::::::");

        return new FlatFileItemReaderBuilder<Persona>().name("personaItemReader")
                .resource(new ClassPathResource("sample-data.csv"))
                .delimited()
                .names("primerNombre", "segundoNombre", "telefono")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Persona>() {
                    {
                        setTargetType(Persona.class);
                    }
                }).build();
    }

    /**
     * Transforma la informacion a mayusculas
     * @return
     */
    @Bean
    public PersonaItemProcessor processor () {
        log.info("Step 3 ::::::::::::::");
        return new PersonaItemProcessor();
    }

    /**
     * Escribir en la BD embebida
     * @param dataSource
     * @return
     */
    @Bean
    public JdbcBatchItemWriter<Persona> writer (DataSource dataSource) {
        log.info("Step 4 ::::::::::::::");
        return new JdbcBatchItemWriterBuilder<Persona>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("INSERT INTO persona (nombre, apaterno, telefono) VALUES (:primerNombre, :segundoNombre, :telefono)")
                .dataSource(dataSource)
                .build();
    }

    /**
     * Pasos a ejecutar
     * @param jobListener
     * @param step1
     * @return
     */
    @Bean
    public Job importPersonJob (JobListener jobListener, Step step1) {
        log.info("Step 0 ::::::::::::::");
        return jobBuilderFactory
                .get("importPersonaJob")
                .incrementer(new RunIdIncrementer())
                .listener(jobListener)
                .flow(step1)
                .end()
                .build();
    }

    /**
     * Paso a ejecutar
     * @param writer
     * @return
     */
    @Bean
    public Step step1 (JdbcBatchItemWriter<Persona> writer) {

        log.info("Step 1 ::::::::::::::");

        return stepBuilderFactory
                .get("step1")
                .<Persona, Persona>chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(writer)
                .build();
    }
}
