package com.pinncode.batchfiledbservice.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * Clase para configuracion para acceso a BD.
 */
@Configuration
public class DBConfiguration {

    /**
     * Configuracion de BD.
     * @return DataSource object
     */
    @Bean(name = "mysqldb")
    @Primary
    @ConfigurationProperties(prefix = "datasource.mysql")
    public DataSource masterDataSource() {
        return DataSourceBuilder.create().build();
    }

    /**
     * Devolver el template con la conexion.
     * @param dsMaster
     * @return JDBC connection.
     */
    @Bean(name = "jdbcConnection")
    @Autowired
    public JdbcTemplate masterJdbcTemplate(@Qualifier("mysqldb") DataSource dsMaster) {
        return new JdbcTemplate(dsMaster);
    }
}
