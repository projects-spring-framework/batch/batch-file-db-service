package com.pinncode.batchfiledbservice.configuration;

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Anula config db para inserciones en tablas batch.
 * Reference:
 * https://stackoverflow.com/questions/33249942/spring-batch-framework-auto-create-batch-table
 * https://stackoverflow.com/questions/25077549/spring-batch-without-persisting-metadata-to-database
 */
@Configuration
@EnableAutoConfiguration
@EnableBatchProcessing
public class BatchDBConfiguration extends DefaultBatchConfigurer {

    @Override
    public void setDataSource(DataSource dataSource) {
    }
}
