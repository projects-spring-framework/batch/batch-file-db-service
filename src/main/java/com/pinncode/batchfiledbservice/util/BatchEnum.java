package com.pinncode.batchfiledbservice.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BatchEnum {

    JOB_ID("job_id", "job_id");

    private final String key;
    private final String value;
}
