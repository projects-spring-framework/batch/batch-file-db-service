package com.pinncode.batchfiledbservice.util;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 */
@Slf4j
public class Util {

    /**
     * Print the log to identify the start of the execution of tasks.
     */
    public static void printLog () {

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss z");

        log.info(" _____         _    _____                     _   _             ");
        log.info("|_   _|_ _ ___| | _| ____|_  _____  ___ _   _| |_(_) ___  _ __  ");
        log.info("  | |/ _` / __| |/ /  _| \\ \\/ / _ \\/ __| | | | __| |/ _ \\| '_ \\ ");
        log.info("  | | (_| \\__ \\   <| |___ >  <  __/ (__| |_| | |_| | (_) | | | |");
        log.info("  |_|\\__,_|___/_|\\_\\_____/_/\\_\\___|\\___|\\__,_|\\__|_|\\___/|_| |_|");
        log.info("=================================== {}\n", format.format(date).toUpperCase());
    }

    public static void printEndLog () {

        log.info(" _____           _ _____         _    ");
        log.info("| ____|_ __   __| |_   _|_ _ ___| | __");
        log.info("|  _| | '_ \\ / _` | | |/ _` / __| |/ /");
        log.info("| |___| | | | (_| | | | (_| \\__ \\   < ");
        log.info("|_____|_| |_|\\__,_| |_|\\__,_|___/_|\\_\\");
    }
}
