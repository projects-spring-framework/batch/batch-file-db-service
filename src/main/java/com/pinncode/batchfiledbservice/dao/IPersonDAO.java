package com.pinncode.batchfiledbservice.dao;

import com.pinncode.batchfiledbservice.model.Persona;

import java.util.List;

public interface IPersonDAO {
    List<Persona> findAll ();
}
