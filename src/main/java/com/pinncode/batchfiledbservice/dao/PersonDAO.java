package com.pinncode.batchfiledbservice.dao;

import com.pinncode.batchfiledbservice.model.Persona;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Reference https://devs4j.com/2018/02/02/utiliza-multiples-bases-de-datos-con-spring-boot-y-spring-jdbc/
 */
@Slf4j
@Component
public class PersonDAO implements IPersonDAO {

    @Autowired
    @Qualifier("jdbcConnection")
    private JdbcTemplate jdbcTemplate;

    public List<Persona> findAll () {

        log.info("ENTRA ------------------");

        final String query = "SELECT nombre,apaterno,telefono FROM PERSONA";

        return jdbcTemplate.query(query, (resultSet, i) -> new Persona(resultSet.getString("nombre"), resultSet.getString("apaterno"), resultSet.getString("telefono")));
    }
}
