package com.pinncode.batchfiledbservice.processor;

import com.pinncode.batchfiledbservice.model.Persona;
import org.springframework.batch.item.ItemProcessor;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.Optional;

import static jodd.util.StringPool.EMPTY;


/**
 * Implementa:
 * ItemProcessor<Persona - Entrada, Persona - Salida>
 */
@Slf4j
public class PersonaItemProcessor implements ItemProcessor<Persona, Persona> {


    @Override
    public Persona process(Persona persona) throws Exception {

        log.info("Step 3.1 ::::::::::::::");

        final String name1 = Optional.ofNullable(persona).map(Persona::getPrimerNombre).map(Objects::toString).orElse(EMPTY);
        final String name2 = Optional.ofNullable(persona).map(Persona::getSegundoNombre).map(Objects::toString).orElse(EMPTY);
        final String telefono = Optional.ofNullable(persona).map(Persona::getTelefono).map(Objects::toString).orElse(EMPTY);

        final Persona newPersona = new Persona(name1.toUpperCase(), name2.toUpperCase(), telefono);

        log.info("Persona converter [{}] to [{}]", persona, newPersona);
        System.out.println("Persona converter :: " + persona + " to :: " + newPersona);

        return newPersona;
    }
}
