package com.pinncode.batchfiledbservice.listener;

import com.pinncode.batchfiledbservice.dao.IPersonDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.pinncode.batchfiledbservice.util.Util.printEndLog;

@Slf4j
@Component
public class JobListener extends JobExecutionListenerSupport {

    @Autowired
    private IPersonDAO personDAO;

    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("End the JOB");

            personDAO.findAll().forEach(persona -> log.info("Persona Rs :: {}", persona));

            printEndLog();
        }
    }
}