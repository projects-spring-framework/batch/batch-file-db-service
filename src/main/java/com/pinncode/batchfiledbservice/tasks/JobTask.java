package com.pinncode.batchfiledbservice.tasks;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import static com.pinncode.batchfiledbservice.util.BatchEnum.JOB_ID;
import static com.pinncode.batchfiledbservice.util.Util.printLog;

/**
 * https://docs.spring.io/spring-batch/docs/current/reference/html/schema-appendix.html#metaDataSchemaOverview
 */
@Slf4j
@Component
public class JobTask {

    /**
     * Simple interface for controlling jobs executions.
     */
    @Autowired
    private JobLauncher jobLauncher;

    /**
     * Batch domain object representing a job.
     */
    @Autowired
    private Job job;

    /**
     * Method that determines the moment in which the execution of tasks is performed.
     */
    @Scheduled(cron = "${batch.configuration.run-job.time}", zone = "${batch.configuration.run-job.time-zone}")
    public void jobExecution () {

        printLog();

        JobParameters jobParameters = new JobParametersBuilder().addString(JOB_ID.getKey(), String.valueOf(System.currentTimeMillis())).toJobParameters();

        try {
            jobLauncher.run(job, jobParameters);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            log.error("Error to execution job :: {}", e.toString());
        }
    }
}