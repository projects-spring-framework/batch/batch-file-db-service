# batch-file-db-service

Prueba batch. Lectura de ficheros y consultas a BD.

Resources:
- https://spring.io/guides/gs/batch-processing/
- https://github.com/4SoftwareDevelopers/demo-spring-batch-scheduling
- https://docs.spring.io/spring-batch/docs/current/reference/html/schema-appendix.html#metaDataSchemaOverview
- https://devs4j.com/2018/02/02/utiliza-multiples-bases-de-datos-con-spring-boot-y-spring-jdbc/
- https://stackoverflow.com/questions/33249942/spring-batch-framework-auto-create-batch-table
- https://stackoverflow.com/questions/25077549/spring-batch-without-persisting-metadata-to-database